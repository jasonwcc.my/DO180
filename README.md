{\rtf1\ansi\deff0\nouicompat{\fonttbl{\f0\fnil\fcharset0 Calibri;}}
{\*\generator Riched20 10.0.19041}\viewkind4\uc1 
\pard\sa200\sl276\slmult1\f0\fs22\lang9 # Red Hat OpenShift I: Containers & Kubernetes \par
### Introduction to building and managing containers for deployment on a Kubernetes and OpenShift 4 cluster\par
Red Hat OpenShift I: Containers & Kubernetes (DO180) introduces students to building and managing containers for deployment on a Kubernetes cluster. This course helps students build core knowledge and skills in managing containers through hands-on experience with containers, Kubernetes, and the Red Hat OpenShift Container Platform needed for multiple roles, including developers, administrators and site reliability engineers.\par
\par
This course is based on Red Hat\'ae Enterprise Linux\'ae 8.2 and OpenShift\'ae Container Platform 4.6.\par
\par
### Set environment variable for speedy lab\par
cat /usr/local/etc/ocp4.config >> ~/.bashrc\par
echo "domain=$\{RHT_OCP4_WILDCARD_DOMAIN\}" >> ~/.bashrc\par
echo 'PS1="\\[\\033[m\\]|\\[\\033[1;35m\\]\\t\\[\\033[m\\]|\\[\\e[1;31m\\]\\u\\[\\e[1;36m\\]\\[\\033[m\\]@\\[\\e[1;36m\\]\\h\\[\\033[m\\]:\\[\\e[0m\\]\\[\\e[1;32m\\]\\n[\\w]> \\[\\e[0m\\]"' >> /.bashrc\par
kubeadminpw=`ssh lab@utility cat ocp4/auth/kubeadmin-pass*`\par
echo "kubeadminpw=$kubeadminpw" >> ~/.bashrc\par
source ~/.bashrc\par
\par
\par
\par
}
 